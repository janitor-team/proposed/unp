# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-31 16:15+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../unp:71
#, perl-format
msgid ""
"\n"
"Usage: %s [OPTION]... [FILE]... -- [TOOL-OPTION]...\n"
"file: compressed file(s) to expand/extract\n"
"\n"
"Use -- [ ARGUMENTS ] to pass arguments to external programs, eg. some tar "
"options:\n"
"unp fastgl.tgz xmnt.tgz -- -C /tmp\n"
"\n"
"Options:\n"
"  -f Continue even if program availability checks fail or directory "
"collision occurs\n"
"  -u Special helper mode.\n"
"\n"
"     For most archive types:\n"
"     a) create directory <filename without suffix>/,\n"
"     b) extract contents there.\n"
"\n"
"     For Debian/Ubuntu packages:\n"
"     a) extract data.tar.gz after each operation in local directory,\n"
"     b) extract control.tar.gz into control/<package_version_arch>/.\n"
"\n"
"  -U Smart mode, acts like -u (see above) if archive contains multiple "
"elements but if there is only one file/directory element then it's stored in "
"the current directory.\n"
"  -s Show the list of supported formats\n"
"  -v More verbosity\n"
"  --version Show the tool version\n"
"  -h Show this help\n"
msgstr ""

#: ../unp:96
#, perl-format
msgid ""
"\n"
"Usage: %s [OPTION]... [FILE]...\n"
"Uncompress multiple files contents to STDOUT.\n"
"\n"
"Options:\n"
"  -s  Show the list of supported formats\n"
"  -h  Show this help\n"
"  -v  More verbosity, to STDERR\n"
"  --version Show the tool version\n"
msgstr ""

#: ../unp:144
msgid "tar with gzip"
msgstr ""

#: ../unp:148
msgid "tar with bzip2"
msgstr ""

#: ../unp:152
msgid "tar with zstd"
msgstr ""

#: ../unp:156
msgid "tar with xz-utils"
msgstr ""

#: ../unp:161
msgid "tar with lzip"
msgstr ""

#: ../unp:166
msgid "tar with lzop"
msgstr ""

#: ../unp:171
msgid "tar with compress"
msgstr ""

#: ../unp:203
msgid "xz-utils or lzma"
msgstr ""

#: ../unp:208
msgid "cpio or afio"
msgstr ""

#: ../unp:213
msgid "rpm2cpio and cpio"
msgstr ""

#: ../unp:217
msgid "formail and mpack"
msgstr ""

#: ../unp:221
msgid "libchm-bin or archmage"
msgstr ""

#: ../unp:226
msgid "rar or unrar or unrar-free"
msgstr ""

#: ../unp:239
msgid "lhasa or lha"
msgstr ""

#: ../unp:269
msgid "p7zip or p7zip-full"
msgstr ""

#: ../unp:301
msgid "maybe orange (or unzip, unrar, unarj, lha)"
msgstr ""

#: ../unp:315
msgid "Known archive formats and tools:\n"
msgstr ""

#: ../unp:341
#, perl-format
msgid ""
"%s (UNP) 2.0~pre10\n"
"Copyright (C) 2023 Eduard Bloch\n"
"License GPLv2: GNU GPL version 2 <https://gnu.org/licenses/gpl.html>.\n"
"This is free software: you are free to change and redistribute it.\n"
"There is NO WARRANTY, to the extent permitted by law.\n"
"\n"
"Written by Andre Karwath and Eduard Bloch.\n"
msgstr ""

#: ../unp:440
msgid "Error, following packages must be installed in order to proceed:\n"
msgstr ""

#: ../unp:475
msgid "Failed to print: "
msgstr ""

#: ../unp:493
#, perl-format
msgid "Cannot read %s, skipping...\n"
msgstr ""

#: ../unp:500
#, perl-format
msgid "Failed to detect file type of %s.\n"
msgstr ""

#: ../unp:550
#, perl-format
msgid ""
"Cannot create target %s: file already exists. Trying alternative targets...\n"
msgstr ""

#: ../unp:556 ../unp:562
#, perl-format
msgid "Cannot create target %s: file already exists\n"
msgstr ""

#: ../unp:631
msgid ""
"Cannot create target directory (already exists), using alternative name\n"
msgstr ""
