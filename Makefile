FPR=cat | md5sum | cut -c1-32
REF=$(shell perl ./ucat test/sample.a | $(FPR))

test:
	test $(REF) = `./ucat test/sample.b | $(FPR)`
	test $(REF) = `./ucat test/sample.c | $(FPR)`
	test $(REF) = `./ucat test/sample.d | $(FPR)`
	test $(REF) = `./ucat test/sample.e | $(FPR)`
	test $(REF) = `./ucat test/sample.f | $(FPR)`

.PHONY: test
